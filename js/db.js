//offline data
db.enablePersistence()
    .then(res => {
        console.log(res)
    })
    .catch(err =>{
        if(err.code == 'failed-precondition'){
            console.log('persistence failed');
        } else if(err.code == 'unimplemented'){
            //lack of browser support
            console.log('presistence is not available');
        }
    });
//real-time listener
db.collection('recipes').onSnapshot((snapshot) => {
    console.log(snapshot.docChanges());
    snapshot.docChanges().forEach(change => {
        console.log(change, change.doc.data(), change.doc.id);
        if(change.type) {
            if(change.type == 'added'){
                renderRecipe(change.doc.data(), change.doc.id);
            }
            if(change.type == 'removed'){
                removeRecipe(change.doc.id);
            }
            if(change.type == 'modified'){
    
            }
        }
    });
});

//add recipe
const form = document.querySelector('form');
form.addEventListener('submit', evt => {
    evt.preventDefault();
    const recipe = {
        title: form.title.value,
        ingredients: form.ingredients.value
    };
    
    db.collection('recipes').add(recipe)
        .catch(err => console.log(err));

    form.title.value = '';
    form.ingredients.value = '';
});

//delete recipe
const recipeContainer = document.querySelector('.recipes');
recipeContainer.addEventListener('click', evt => {
    // console.log(evt.target.tagName);
    // console.log(evt.target.textContent);
    // console.log(evt.target.outerHTML);
    if(evt.target.tagName === 'I'){
        const id = evt.target.getAttribute('data-id');
        db.collection('recipes').doc(id).delete();
    }

});